# Portuguese translation for Debian's release-notes
# 2006-11-06 - Rui Branco <ruipb@debianpt.org> - initial translation
# Released under the same license as the release notes.
#
# Rui Branco <ruipb@debianpt.org>, 2006-2007.
# Ricardo Silva <ardoric@gmail.com>, 2007.
# Miguel Figueiredo <elmig@debianpt.org>, 2007.
# António Moreira <antoniocostamoreira@gmail.com>, 2009.
# Américo Monteiro <a_monteiro@netcabo.pt>, 2009, 2011.
# Miguel Figueiredo <elmig@debianpt.org>, 2012, 2013, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version:  release-notes\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-06 00:00+0200\n"
"PO-Revision-Date: 2021-06-06 16:27+0100\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language: pt\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../index.rst:2
msgid "Release Notes for Debian |RELEASE| (|RELEASENAME|\\)"
msgstr "Notas de Lançamento para Debian |RELEASE| (|RELEASENAME|\\)"

#: ../index.rst:4
msgid ""
"The Debian Documentation Project `<https://www.debian.org/doc> "
"<https://www.debian.org/doc>`__."
msgstr ""

#: ../index.rst:6
msgid ""
"This document is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License, version 2, as "
"published by the Free Software Foundation."
msgstr ""
"Este documento é software livre; você pode redistribuí-lo e/ou "
"modificá-lo sob os termos da GNU General Public License, versão 2, "
"conforme publicado pela Free Software Foundation."

#: ../index.rst:10
msgid ""
"This program is distributed in the hope that it will be useful, but "
"WITHOUT ANY WARRANTY; without even the implied warranty of "
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General"
" Public License for more details."
msgstr ""
"Este programa é distribuído na esperança de que seja útil, mas SEM "
"QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIABILIDADE ou "
"ADEQUAÇÃO A UM PROPÓSITO PARTICULAR. Veja a GNU General Public License "
"para mais detalhes."

#: ../index.rst:15
msgid ""
"You should have received a copy of the GNU General Public License along "
"with this program; if not, write to the Free Software Foundation, Inc., "
"51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."
msgstr ""
"Você deve ter recebido uma cópia da GNU General Public License juntamente"
" com este programa; caso contrário, escreva para a Free Software "
"Foundation, Inc.,51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 "
"USA."

#: ../index.rst:19
msgid ""
"The license text can also be found at "
"`<https://www.gnu.org/licenses/gpl-2.0.html>`__ and ``/usr/share/common-"
"licenses/GPL-2`` on Debian systems."
msgstr ""
"O texto de licença também pode ser encontrado em "
"`<https://www.gnu.org/licenses/gpl-2.0.html>`__ e ``/usr/share/common-"
"licenses/GPL-2`` nos sistemas Debian."
