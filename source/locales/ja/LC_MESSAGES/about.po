# Japanese translations for Debian release notes
# Debian リリースノートの日本語訳
# Noritada Kobayashi <nori1@dolphin.c.u-tokyo.ac.jp>, 2006.
# KURASAWA Nozomu <nabetaro@caldron.jp>, 2009.
# Hideki Yamane <henrich@debian.org>, 2010, 2013, 2015-2021.
# hoxp18 <hoxp18@noramail.jp>, 2019.
# This file is distributed under the same license as Debian release notes.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 11\n"
"Report-Msgid-Bugs-To: debian-doc@lists.debian.org\n"
"POT-Creation-Date: 2023-11-05 13:18+0100\n"
"PO-Revision-Date: 2021-07-27 15:27+0900\n"
"Last-Translator: Hideki Yamane <henrich@debian.org>\n"
"Language: ja\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Plural-Forms: nplurals=1; plural=0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../about.rst:4
msgid "Introduction"
msgstr "はじめに"

#: ../about.rst:6
msgid ""
"This document informs users of the Debian distribution about major "
"changes in version |RELEASE| (codenamed |RELEASENAME|)."
msgstr ""
"この文書は Debian ディストリビューションのユーザーに、バージョン |RELEASE| (コードネーム |RELEASENAME|) "
"での大きな変更点を知らせるものです。"

#: ../about.rst:9
msgid ""
"The release notes provide information on how to upgrade safely from "
"release |OLDRELEASE| (codenamed |OLDRELEASENAME|) to the current release "
"and inform users of known potential issues they could encounter in that "
"process."
msgstr ""
"このリリースノートでは、リリース |OLDRELEASE| (コードネーム |OLDRELEASENAME|) "
"から今回のリリースへの安全なアップグレード方法や、その際ユーザーが遭遇する可能性がある既知の問題点についての情報をユーザーに提供しています。"

#: ../about.rst:14
#, fuzzy
msgid ""
"You can get the most recent version of this document from "
"|URL-R-N-STABLE|."
msgstr "この文書の最新版は、|URL-R-N-STABLE| から取得できます。"

#: ../about.rst:19
msgid ""
"Note that it is impossible to list every known issue and that therefore a"
" selection has been made based on a combination of the expected "
"prevalence and impact of issues."
msgstr "既知の問題点をすべて列挙するのは不可能なので、問題点の予想される広がり具合と影響の大きさの双方に基づいて取捨選択していることに注意してください。"

#: ../about.rst:23
msgid ""
"Please note that we only support and document upgrading from the previous"
" release of Debian (in this case, the upgrade from |OLDRELEASENAME|). If "
"you need to upgrade from older releases, we suggest you read previous "
"editions of the release notes and upgrade to |OLDRELEASENAME| first."
msgstr ""
"Debian の 1 つ前のリリースからのアップグレード (この場合、|OLDRELEASENAME| からのアップグレード) "
"のみがサポート・記述されていることに注意してください。さらに古いリリースからのアップグレードが必要な場合は、過去のリリースノートを読み、まず "
"|OLDRELEASENAME| へとアップグレードすることをお勧めします。"

#: ../about.rst:32
msgid "Reporting bugs on this document"
msgstr "この文書に関するバグを報告する"

#: ../about.rst:34
msgid ""
"We have attempted to test all the different upgrade steps described in "
"this document and to anticipate all the possible issues our users might "
"encounter."
msgstr "私たちは、この文書で説明されているすべての異なるアップグレード手順を試し、また、ユーザーが直面する可能性のある、すべての問題を想定しました。"

#: ../about.rst:38
#, fuzzy
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or"
" information that is missing) in this documentation, please file a bug in"
" the `bug tracking system <https://bugs.debian.org/>`__ against the "
"**release-notes** package. You might first want to review the `existing "
"bug reports <https://bugs.debian.org/release-notes>`__ in case the issue "
"you've found has already been reported. Feel free to add additional "
"information to existing bug reports if you can contribute content for "
"this document."
msgstr ""
"それにも関わらず、この文書にバグ (不正確な情報や抜け落ちている情報) を見つけたと思う場合には、**release-notes** "
"パッケージに対するバグ報告として、`バグ追跡システム<https://bugs.debian.org/>`__に提出してください。あなたが発見した問題が既に報告されている場合に備え、まずは`既存のバグ報告<https://bugs.debian.org"
"/release-"
"notes>`__を確認してみると良いでしょう。もしこの文書にさらに内容を付加できるのであれば、どうぞ遠慮なく既存のバグ報告へ情報を追加して下さい。"

#: ../about.rst:47
#, fuzzy
msgid ""
"We appreciate, and encourage, reports providing patches to the document's"
" sources. You will find more information describing how to obtain the "
"sources of this document in `Sources for this document <#sources>`__."
msgstr ""
"私たちは、この文書のソースへのパッチを含めた報告を歓迎・推奨します。このドキュメントのソースの取得方法の記述については `Sources for "
"this document <#sources>`__ で、より詳細な情報を見つけることができるでしょう。"

#: ../about.rst:55
msgid "Contributing upgrade reports"
msgstr "アップグレードについての報告をする"

#: ../about.rst:57
#, fuzzy
msgid ""
"We welcome any information from users related to upgrades from "
"|OLDRELEASENAME| to |RELEASENAME|. If you are willing to share "
"information please file a bug in the `bug tracking system "
"<https://bugs.debian.org/>`__ against the **upgrade-reports** package "
"with your results. We request that you compress any attachments that are "
"included (using ``gzip``)."
msgstr ""
"|OLDRELEASENAME| から |RELEASENAME| "
"へのアップグレードに関連するユーザーからの情報はどんなものでも歓迎します。情報を共有するのを厭わない場合は、**upgrade-reports**"
" パッケージに対するバグ報告として、アップグレードの結果を含めて`バグ追跡システム<https://bugs.debian.org/>`__ "
"に提出してください。報告に添付ファイルを含める場合は、(``gzip`` を使用して) 圧縮するようお願いします。"

#: ../about.rst:63
msgid ""
"Please include the following information when submitting your upgrade "
"report:"
msgstr "アップグレードについての報告を提出する際には、以下の情報を含めてください。"

#: ../about.rst:66
#, fuzzy
msgid ""
"The status of your package database before and after the upgrade: "
"**dpkg**'s status database available at ``/var/lib/dpkg/status`` and "
"**apt**'s package state information, available at "
"``/var/lib/apt/extended_states``. You should have made a backup before "
"the upgrade as described at :ref:`data-backup`, but you can also find "
"backups of ``/var/lib/dpkg/status`` in ``/var/backups``."
msgstr ""
"アップグレード前後のパッケージデータベースの状態。``/var/lib/dpkg/status`` にある **dpkg** "
"の状態データベースと、``/var/lib/apt/extended_states`` にある **apt** のパッケージ状態情報です。:ref"
":`data-backup` "
"で説明するように、アップグレードを実行する前にバックアップをとっておくべきですが、``/var/lib/dpkg/status`` "
"のバックアップは ``/var/backups`` にもあります。"

#: ../about.rst:74
msgid ""
"Session logs created using ``script``, as described in :ref:`record-"
"session`."
msgstr "``script`` を使用して作成したセッションのログ。:ref:`record-session` で説明します。"

#: ../about.rst:77
msgid ""
"Your ``apt`` logs, available at ``/var/log/apt/term.log``, or your "
"``aptitude`` logs, available at ``/var/log/aptitude``."
msgstr ""
"``/var/log/apt/term.log`` にある ``apt`` のログか、``/var/log/aptitude`` にある "
"``aptitude`` のログ。"

#: ../about.rst:82
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug "
"report as the information will be published in a public database."
msgstr "バグ報告に情報を含める前に、慎重に扱うべき情報や機密情報がログに含まれていないかある程度時間をかけて検査し、ログから削除してください。なぜなら、バグ報告に含まれる情報は公開データベースで公表されるからです。"

#: ../about.rst:89
msgid "Sources for this document"
msgstr "この文書のソース"

#: ../about.rst:91
#, fuzzy
msgid ""
"The source of this document is in reStructuredText format, using the "
"sphinx converter. The HTML version is generated using *sphinx-build -b "
"html*. The PDF version is generated using *sphinx-build -b latex*. "
"Sources for the Release Notes are available in the Git repository of the "
"*Debian Documentation Project*. You can use the `web interface "
"<https://salsa.debian.org/ddp-team/release-notes/>`__ to access its files"
" individually through the web and see their changes. For more information"
" on how to access Git please consult the `Debian Documentation Project "
"VCS information pages <https://www.debian.org/doc/vcs>`__."
msgstr ""
"この文書のソースは DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> 形式です。HTML 版は、<systemitem role=\"package"
"\">docbook-xsl</systemitem> と <systemitem "
"role=\"package\">xsltproc</systemitem> を使用して生成しています。PDF 版は、<systemitem "
"role=\"package\">dblatex</systemitem> や <systemitem "
"role=\"package\">xmlroff</systemitem> を使用して生成しています。リリースノートのソースは Debian "
"ドキュメンテーションプロジェクト (*Debian Documentation Project*) の Git "
"リポジトリにあります。ウェブから`ウェブインターフェース<https://salsa.debian.org/ddp-team/release-"
"notes/>`__を使って個々のファイルにアクセスでき、変更を参照できます。Git "
"へのアクセス方法に関してさらに詳しく知りたい場合は、`Debian ドキュメンテーションプロジェクトの VCS "
"情報ページ<https://www.debian.org/doc/vcs>`__を参照してください。"

