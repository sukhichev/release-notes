# Catalan translation of the Debian Release Notes
#
# Miguel Gea Milvaques, 2006-2009.
# Jordà Polo, 2007, 2009.
# Guillem Jover <guillem@debian.org>, 2007, 2019.
# Héctor Orón Martínez, 2011.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-11-05 13:18+0100\n"
"PO-Revision-Date: 2020-12-09 15:01+0100\n"
"Last-Translator: Alex Muntada <alexm@debian.org>\n"
"Language: ca\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../old-stuff.rst:4
msgid "Managing your |OLDRELEASENAME| system before the upgrade"
msgstr "Gestió del vostre sistema |OLDRELEASENAME| abans de l'actualització"

#: ../old-stuff.rst:6
msgid ""
"This appendix contains information on how to make sure you can install or"
" upgrade |OLDRELEASENAME| packages before you upgrade to |RELEASENAME|. "
"This should only be necessary in specific situations."
msgstr ""
"Este apèndix conté informació de com instal·lar o actualitzar els paquets"
" de |OLDRELEASENAME| abans d'actualitzar a |RELEASENAME|. Açò tan sols "
"serà necessari en algunes situacions concretes."

#: ../old-stuff.rst:13
msgid "Upgrading your |OLDRELEASENAME| system"
msgstr "Actualització del vostre sistema |OLDRELEASENAME|"

#: ../old-stuff.rst:15
#, fuzzy
msgid ""
"Basically this is no different from any other upgrade of |OLDRELEASENAME|"
" you've been doing. The only difference is that you first need to make "
"sure your package list still contains references to |OLDRELEASENAME| as "
"explained in `Checking your APT source-list files <#old-sources>`__."
msgstr ""
"Bàsicament no hi ha diferències entre qualsevol altra actualització de "
"|OLDRELEASENAME| que ja hàgiu fet. L'única diferència és que necessitareu"
" primer assegurar-vos que la vostra llista de paquets encara conté "
"referències a |OLDRELEASENAME| tal com s'explica a `Checking your APT "
"source-list files <#old-sources>`__."

#: ../old-stuff.rst:20
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically "
"be upgraded to the latest |OLDRELEASENAME| point release."
msgstr ""
"Si actualitzeu el vostre sistema utilitzant una rèplica de Debian, "
"s'actualitzarà de forma automàtica a l'últim revisió del llançament de "
"|OLDRELEASENAME|."

#: ../old-stuff.rst:26
msgid "Checking your APT source-list files"
msgstr "Comprovació dels fitxers de llista de fonts d'APT"

#: ../old-stuff.rst:28
#, fuzzy
msgid ""
"If any of the lines in your APT source-list files (see :url-man-"
"stable:`sources.list(5)`) contain references to \"stable\", this is "
"effectively pointing to |RELEASENAME| already. This might not be what you"
" want if you are not yet ready for the upgrade. If you have already run "
"``apt update``, you can still get back without problems by following the "
"procedure below."
msgstr ""
"Si alguna de les línies al vostre /etc/apt/sources.list es parlen "
"d'\"stable\", aleshores efectivament ja esteu \"utilitzant\" "
"|RELEASENAME|. Això podria ser el que no vols si no ets a punt encara per"
" l'actualització.  Si ja has executat ``apt update``, encara podreu tirar"
" enrere sense problemes seguint el següent procediment."

#: ../old-stuff.rst:35
#, fuzzy
msgid ""
"If you have also already installed packages from |RELEASENAME|, there "
"probably is not much point in installing packages from |OLDRELEASENAME| "
"anymore. In that case you will have to decide for yourself whether you "
"want to continue or not. It is possible to downgrade packages, but that "
"is not covered here."
msgstr ""
"Si heu instal·lat paquets de |RELEASENAME|, possiblement no té cap sentit"
" continuar instal·lant paquets de |OLDRELEASENAME|.  De qualsevol manera,"
" haureu de decidir vosaltres si voleu continuar o no. És possible baixar "
"la versió dels paquets, però no hi donem suport ací."

#: ../old-stuff.rst:41
#, fuzzy
msgid ""
"As root, open the relevant APT source-list file (such as "
"``/etc/apt/sources.list``) with your favorite editor, and check all lines"
" beginning with"
msgstr ""
"Obriu el vostre fitxers ``/etc/apt/sources.list`` amb el vostre editor "
"preferit (com a root) i comproveu totes les línies que comencen amb"

#: ../old-stuff.rst:45
msgid "``deb http:``"
msgstr ""

#: ../old-stuff.rst:46
msgid "``deb https:``"
msgstr ""

#: ../old-stuff.rst:47
msgid "``deb tor+http:``"
msgstr ""

#: ../old-stuff.rst:48
msgid "``deb tor+https:``"
msgstr ""

#: ../old-stuff.rst:49
msgid "``URIs: http:``"
msgstr ""

#: ../old-stuff.rst:50
msgid "``URIs: https:``"
msgstr ""

#: ../old-stuff.rst:51
msgid "``URIs: tor+http:``"
msgstr ""

#: ../old-stuff.rst:52
msgid "``URIs: tor+https:``"
msgstr ""

#: ../old-stuff.rst:54
#, fuzzy
msgid ""
"for a reference to \"stable\". If you find any, change \"stable\" to "
"\"|OLDRELEASENAME|\"."
msgstr ""
"amb referències a \"stable\". Si en trobeu cap, canvieu \"stable\" per "
"\"|OLDRELEASENAME|\"."

#: ../old-stuff.rst:56
#, fuzzy
msgid ""
"If you have any lines starting with ``deb file:`` or ``URIs: file:``, you"
" will have to check for yourself if the location they refer to contains a"
" |OLDRELEASENAME| or |RELEASENAME| archive."
msgstr ""
"Si teniu alguna línia que comença per ``deb file:``, haureu de comprovar "
"vosaltres mateix el lloc al que s'haureu d'adreçar que continga l'arxiu "
"|OLDRELEASENAME| o un de |RELEASENAME|."

#: ../old-stuff.rst:62
#, fuzzy
msgid ""
"Do not change any lines that begin with ``deb cdrom:`` or ``URIs: "
"cdrom:``. Doing so would invalidate the line and you would have to run "
"``apt-cdrom`` again. Do not be alarmed if a ``cdrom:`` source line refers"
" to \"unstable\". Although confusing, this is normal."
msgstr ""
"No canvieu cap de les línies que comencen amb ``deb cdrom:``.  Si ho feu,"
" invalidareu la línia i haureu d'executar ``apt-cdrom`` altra vegada. No "
"us preocupeu si la línia de ``cdrom:`` apunta a \"unstable\".  Malgrat "
"que pot confondre, és normal."

#: ../old-stuff.rst:68
msgid "If you've made any changes, save the file and execute"
msgstr "Si heu fet algun canvi, guardeu el fitxer i executeu"

#: ../old-stuff.rst:74
msgid "to refresh the package list."
msgstr "per refrescar la llista de paquets."

#: ../old-stuff.rst:79
msgid "Removing obsolete configuration files"
msgstr "Eliminar fitxers de configuració obsolets"

#: ../old-stuff.rst:81
#, fuzzy
msgid ""
"Before upgrading your system to |RELEASENAME|, it is recommended to "
"remove old configuration files (such as ``*.dpkg-{new,old}`` files under "
"``/etc``) from the system."
msgstr ""
"Abans d'actualitzar el vostre sistema a |RELEASENAME| és recomanable "
"treure els fitxers de configuració antics (com ara ``*.dpkg-{new,old}`` a"
" sota el directori ``/etc``) del sistema."

