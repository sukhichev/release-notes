# Slovak messages for release-notes.
# Copyright (C) 2009 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2013, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-11-05 13:18+0100\n"
"PO-Revision-Date: 2017-06-16 15:21+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language: sk\n"
"Language-Team: x\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../whats-new.rst:4
msgid "What's new in Debian |RELEASE|"
msgstr "Čo je nové v Debian |RELEASE|"

#: ../whats-new.rst:6
#, fuzzy
msgid ""
"The `Wiki <https://wiki.debian.org/NewInBookworm>`__ has more information"
" about this topic."
msgstr ""
"`Wiki <https://wiki.debian.org/NewInBookworm>`__ obsahuje ďalšie "
"informácie na túto tému."

#: ../whats-new.rst:12
msgid "Supported architectures"
msgstr "Podporované architektúry"

#: ../whats-new.rst:14
msgid ""
"The following are the officially supported architectures for Debian "
"|RELEASE|:"
msgstr "Debian |RELEASENAME| oficiálne podporuje nasledovné architektúry:"

#: ../whats-new.rst:17
msgid "32-bit PC (``i386``) and 64-bit PC (``amd64``)"
msgstr ""

#: ../whats-new.rst:19
msgid "64-bit ARM (``arm64``)"
msgstr ""

#: ../whats-new.rst:21
msgid "ARM EABI (``armel``)"
msgstr ""

#: ../whats-new.rst:23
msgid "ARMv7 (EABI hard-float ABI, ``armhf``)"
msgstr ""

#: ../whats-new.rst:25
msgid "little-endian MIPS (``mipsel``)"
msgstr ""

#: ../whats-new.rst:27
msgid "64-bit little-endian MIPS (``mips64el``)"
msgstr ""

#: ../whats-new.rst:29
msgid "64-bit little-endian PowerPC (``ppc64el``)"
msgstr ""

#: ../whats-new.rst:31
msgid "IBM System z (``s390x``)"
msgstr ""

#: ../whats-new.rst:36
msgid "Baseline bump for 32-bit PC to i686"
msgstr ""

#: ../whats-new.rst:34
msgid ""
"The 32-bit PC support (known as the Debian architecture i386) now "
"requires the \"long NOP\" instruction. Please refer to "
":ref:`i386-is-i686` for more information."
msgstr ""

#: ../whats-new.rst:38
msgid ""
"You can read more about port status, and port-specific information for "
"your architecture at the `Debian port web pages "
"<https://www.debian.org/ports/>`__."
msgstr ""
"Ďalšie informácie o stave portov a informácie špecifické pre vašu "
"architektúru sa dočítate na `stránkach portov Debianu "
"<https://www.debian.org/ports/>`__."

#: ../whats-new.rst:44
msgid "Archive areas"
msgstr ""

#: ../whats-new.rst:46
msgid ""
"The following archive areas, mentioned in the Social Contract and in the "
"Debian Policy, have been around for a long time:"
msgstr ""

#: ../whats-new.rst:49
#, fuzzy
msgid "main: the Debian distribution;"
msgstr "Čo je nové v distribúcii?"

#: ../whats-new.rst:51
msgid ""
"contrib: supplemental packages intended to work with the Debian "
"distribution, but which require software outside of the distribution to "
"either build or function;"
msgstr ""

#: ../whats-new.rst:55
msgid ""
"non-free: supplemental packages intended to work with the Debian "
"distribution that do not comply with the DFSG or have other problems that"
" make their distribution problematic."
msgstr ""

#: ../whats-new.rst:59
msgid ""
"Following the `2022 General Resolution about non-free firmware "
"<https://www.debian.org/vote/2022/vote_003>`__, the 5th point of the "
"Social Contract was extended with the following sentence:"
msgstr ""

#: ../whats-new.rst:63
msgid ""
"The Debian official media may include firmware that is otherwise not part"
" of the Debian system to enable use of Debian with hardware that requires"
" such firmware."
msgstr ""

#: ../whats-new.rst:67
msgid ""
"While it's not mentioned explicitly in either the Social Contract or "
"Debian Policy yet, a new archive area was introduced, making it possible "
"to separate non-free firmware from the other non-free packages:"
msgstr ""

#: ../whats-new.rst:71
msgid "non-free-firmware"
msgstr ""

#: ../whats-new.rst:73
msgid ""
"Most non-free firmware packages have been moved from ``non-free`` to "
"``non-free-firmware`` in preparation for the Debian |RELEASE| release. "
"This clean separation makes it possible to build official installation "
"images with packages from ``main`` and from ``non-free-firmware``, "
"without ``contrib`` or ``non-free``. In turn, these installation images "
"make it possible to install systems with only ``main`` and ``non-free-"
"firmware``, without ``contrib`` or ``non-free``."
msgstr ""

#: ../whats-new.rst:81
msgid "See :ref:`non-free-firmware` for upgrades from |OLDRELEASENAME|."
msgstr ""

#: ../whats-new.rst:86
msgid "What's new in the distribution?"
msgstr "Čo je nové v distribúcii?"

#: ../whats-new.rst:91
#, fuzzy, python-format
msgid ""
"This new release of Debian again comes with a lot more software than its "
"predecessor |OLDRELEASENAME|; the distribution includes over |PACKAGES-"
"NEW| new packages, for a total of over |PACKAGES-TOTAL| packages. Most of"
" the software in the distribution has been updated: over |PACKAGES-"
"UPDATED| software packages (this is |PACKAGES-UPDATE-PERCENT|\\% of all "
"packages in |OLDRELEASENAME|). Also, a significant number of packages "
"(over |PACKAGES-REMOVED|, |PACKAGES-REMOVED-PERCENT|\\% of the packages "
"in |OLDRELEASENAME|) have for various reasons been removed from the "
"distribution. You will not see any updates for these packages and they "
"will be marked as \"obsolete\" in package management front-ends; see "
":ref:`obsolete`."
msgstr ""
"Toto nové vydanie Debianu opäť prináša omnoho viac softvéru ako jeho "
"predchodca  |OLDRELEASENAME|; distribúcia obsahuje viac ako |PACKAGES-"
"NEW| nových balíkov, čo je celkovo viac ako |PACKAGES-TOTAL| balíkov. "
"Väčšina softvéru v distribúcii bola aktualizovaná: viac ako |PACKAGES-"
"UPDATED| softvérových balíkov (to predstavuje |PACKAGES-UPDATE-"
"PERCENT|\\% všetkých balíkov v  |OLDRELEASENAME|). Rovnako bolo z rôznych"
" dôvodov z distribúcie odstránené významné množstvo balíkov (viac ako "
"|PACKAGES-REMOVED|, |PACKAGES-REMOVED-PERCENT|\\% balíkov v  "
"|OLDRELEASENAME|). Neuvidíte žiadne aktualizácie týchto balíkov a v "
"systémoch na správu balíkov budú označené ako \"zastaralé\"; pozri "
":ref:`obsolete`."

#: ../whats-new.rst:106
msgid "Desktops and well known packages"
msgstr ""

#: ../whats-new.rst:108
msgid ""
"Debian again ships with several desktop applications and environments. "
"Among others it now includes the desktop environments GNOME 43, KDE "
"Plasma 5.27, LXDE 11, LXQt 1.2.0, MATE 1.26, and Xfce 4.18."
msgstr ""
"Debian sa znova dodáva s niekoľkými pracovnými prostrediami a "
"aplikáciami. Okrem iných teraz obsahuje pracovné prostredia GNOME 43, KDE"
" Plasma 5.27, LXDE 11, LXQt 1.2.0, MATE 1.26 a Xfce 4.18."

#: ../whats-new.rst:112
msgid ""
"Productivity applications have also been upgraded, including the office "
"suites:"
msgstr ""
"Kancelárske aplikácie tiež boli aktualizované, vrátane kancelárskych "
"balíkov:"

#: ../whats-new.rst:115
msgid "LibreOffice is upgraded to version 7.4;"
msgstr "LibreOffice je aktualizovaný na 7.4;"

#: ../whats-new.rst:117
msgid "GNUcash is upgraded to 4.13;"
msgstr "GNUcash je aktualizovaný na 4.13;"

#: ../whats-new.rst:119
msgid ""
"Among many others, this release also includes the following software "
"updates:"
msgstr ""
"Okrem množstva ďalších obsahuje toto vydanie aj nasledovné aktualizácie "
"softvéru:"

#: ../whats-new.rst:123
msgid "Package"
msgstr "Balík"

#: ../whats-new.rst:123
msgid "Version in |OLDRELEASE| (|OLDRELEASENAME|)"
msgstr "Verzia v |OLDRELEASE| (|OLDRELEASENAME|)"

#: ../whats-new.rst:123
msgid "Version in |RELEASE| (|RELEASENAME|)"
msgstr "Verzia v |RELEASE| (|RELEASENAME|)"

#: ../whats-new.rst:127
msgid "Apache"
msgstr ""

# | msgid "2.4.40"
#: ../whats-new.rst:127
#, fuzzy
msgid "2.4.54"
msgstr "2.4.40"

# | msgid "2.4.40"
#: ../whats-new.rst:127 ../whats-new.rst:163
#, fuzzy
msgid "2.4.57"
msgstr "2.4.40"

#: ../whats-new.rst:129
msgid "Bash"
msgstr ""

# | msgid "2.1"
#: ../whats-new.rst:129
#, fuzzy
msgid "5.1"
msgstr "2.1"

# | msgid "2.2.16"
#: ../whats-new.rst:129
#, fuzzy
msgid "5.2.15"
msgstr "2.2.16"

#: ../whats-new.rst:131
msgid "BIND DNS Server"
msgstr ""

#: ../whats-new.rst:131
msgid "9.16"
msgstr ""

#: ../whats-new.rst:131
msgid "9.18"
msgstr ""

#: ../whats-new.rst:133
msgid "Cryptsetup"
msgstr ""

# | msgid "2.1"
#: ../whats-new.rst:133
#, fuzzy
msgid "2.3"
msgstr "2.1"

# | msgid "2.4.40"
#: ../whats-new.rst:133
#, fuzzy
msgid "2.6"
msgstr "2.4.40"

#: ../whats-new.rst:135
msgid "Emacs"
msgstr "Emacs"

#: ../whats-new.rst:135
msgid "27.1"
msgstr ""

# | msgid "2.24"
#: ../whats-new.rst:135
#, fuzzy
msgid "28.2"
msgstr "2.24"

#: ../whats-new.rst:137
msgid "Exim default e-mail server"
msgstr ""

#: ../whats-new.rst:137
msgid "4.94"
msgstr ""

#: ../whats-new.rst:137
#, fuzzy
msgid "4.96"
msgstr "4.9"

#: ../whats-new.rst:140
msgid "GNU Compiler Collection as default compiler"
msgstr ""

#: ../whats-new.rst:140
msgid "10.2"
msgstr ""

# | msgid "1.0.8"
#: ../whats-new.rst:140
#, fuzzy
msgid "12.2"
msgstr "1.0.8"

#: ../whats-new.rst:144
msgid "GIMP"
msgstr ""

#: ../whats-new.rst:144
msgid "2.10.22"
msgstr ""

# | msgid "2.4.31"
#: ../whats-new.rst:144
#, fuzzy
msgid "2.10.34"
msgstr "2.4.31"

#: ../whats-new.rst:146
msgid "GnuPG"
msgstr ""

# | msgid "2.2.22"
#: ../whats-new.rst:146
#, fuzzy
msgid "2.2.27"
msgstr "2.2.22"

# | msgid "2.2.22"
#: ../whats-new.rst:146
#, fuzzy
msgid "2.2.40"
msgstr "2.2.22"

#: ../whats-new.rst:148
msgid "Inkscape"
msgstr ""

# | msgid "1.0.8"
#: ../whats-new.rst:148
#, fuzzy
msgid "1.0.2"
msgstr "1.0.8"

# | msgid "1.0.8"
#: ../whats-new.rst:148
#, fuzzy
msgid "1.2.2"
msgstr "1.0.8"

#: ../whats-new.rst:150
msgid "the GNU C library"
msgstr ""

#: ../whats-new.rst:150
msgid "2.31"
msgstr ""

# | msgid "2.1"
#: ../whats-new.rst:150
#, fuzzy
msgid "2.36"
msgstr "2.1"

#: ../whats-new.rst:152
msgid "Linux kernel image"
msgstr "Obraz linuxového jadra"

# | msgid "4.9 series"
#: ../whats-new.rst:152
#, fuzzy
msgid "5.10 series"
msgstr "séria 4.9"

# | msgid "4.9 series"
#: ../whats-new.rst:152
#, fuzzy
msgid "6.1 series"
msgstr "séria 4.9"

#: ../whats-new.rst:154
msgid "LLVM/Clang toolchain"
msgstr ""

#: ../whats-new.rst:154
msgid "9.0.1 and 11.0.1 (default) and 13.0.1"
msgstr ""

#: ../whats-new.rst:154
msgid "13.0.1 and 14.0 (default) and 15.0.6"
msgstr ""

#: ../whats-new.rst:157
msgid "MariaDB"
msgstr ""

#: ../whats-new.rst:157
msgid "10.5"
msgstr ""

# | msgid "1.0.8"
#: ../whats-new.rst:157
#, fuzzy
msgid "10.11"
msgstr "1.0.8"

#: ../whats-new.rst:159
msgid "Nginx"
msgstr ""

#: ../whats-new.rst:159
msgid "1.18"
msgstr ""

# | msgid "1.0.8"
#: ../whats-new.rst:159
#, fuzzy
msgid "1.22"
msgstr "1.0.8"

#: ../whats-new.rst:161
msgid "OpenJDK"
msgstr ""

# | msgid "10.1"
#: ../whats-new.rst:161
#, fuzzy
msgid "11"
msgstr "10.1"

#: ../whats-new.rst:161
msgid "17"
msgstr ""

#: ../whats-new.rst:163
msgid "OpenLDAP"
msgstr ""

# | msgid "2.4.31"
#: ../whats-new.rst:163
#, fuzzy
msgid "2.5.13"
msgstr "2.4.31"

#: ../whats-new.rst:165
msgid "OpenSSH"
msgstr ""

# | msgid "7.4p1"
#: ../whats-new.rst:165
#, fuzzy
msgid "8.4p1"
msgstr "7.4p1"

# | msgid "7.4p1"
#: ../whats-new.rst:165
#, fuzzy
msgid "9.2p1"
msgstr "7.4p1"

#: ../whats-new.rst:167
msgid "OpenSSL"
msgstr ""

#: ../whats-new.rst:167
msgid "1.1.1n"
msgstr ""

# | msgid "0.7.2"
#: ../whats-new.rst:167
#, fuzzy
msgid "3.0.8"
msgstr "0.7.2"

#: ../whats-new.rst:169
msgid "Perl"
msgstr ""

#: ../whats-new.rst:169
msgid "5.32"
msgstr ""

#: ../whats-new.rst:169
msgid "5.36"
msgstr ""

#: ../whats-new.rst:171
msgid "PHP"
msgstr ""

# | msgid "7.4p1"
#: ../whats-new.rst:171
#, fuzzy
msgid "7.4"
msgstr "7.4p1"

# | msgid "2.24"
#: ../whats-new.rst:171 ../whats-new.rst:185
#, fuzzy
msgid "8.2"
msgstr "2.24"

#: ../whats-new.rst:173
msgid "Postfix MTA"
msgstr ""

#: ../whats-new.rst:173
msgid "3.5"
msgstr "3.5"

# | msgid "0.7.2"
#: ../whats-new.rst:173
#, fuzzy
msgid "3.7"
msgstr "0.7.2"

#: ../whats-new.rst:175
msgid "PostgreSQL"
msgstr ""

#: ../whats-new.rst:175
msgid "13"
msgstr ""

#: ../whats-new.rst:175
msgid "15"
msgstr ""

#: ../whats-new.rst:177
msgid "Python 3"
msgstr "Python 3"

# | msgid "0.7.2"
#: ../whats-new.rst:177
#, fuzzy
msgid "3.9.2"
msgstr "0.7.2"

#: ../whats-new.rst:177
msgid "3.11.2"
msgstr ""

#: ../whats-new.rst:179
msgid "Rustc"
msgstr ""

#: ../whats-new.rst:179
msgid "1.48"
msgstr ""

# | msgid "1.4.31"
#: ../whats-new.rst:179
#, fuzzy
msgid "1.63"
msgstr "1.4.31"

#: ../whats-new.rst:181
msgid "Samba"
msgstr "Samba"

#: ../whats-new.rst:181
msgid "4.13"
msgstr ""

#: ../whats-new.rst:181
msgid "4.17"
msgstr ""

#: ../whats-new.rst:183
msgid "Systemd"
msgstr ""

#: ../whats-new.rst:183
msgid "247"
msgstr ""

#: ../whats-new.rst:183
msgid "252"
msgstr ""

#: ../whats-new.rst:185
msgid "Vim"
msgstr "Vim"

#: ../whats-new.rst:185
msgid "9.0"
msgstr ""

#: ../whats-new.rst:191
msgid "More translated man pages"
msgstr ""

#: ../whats-new.rst:193
msgid ""
"Thanks to our translators, more documentation in ``man``-page format is "
"available in more languages than ever. For example, many man pages are "
"now available in Czech, Danish, Greek, Finnish, Indonesian, Macedonian, "
"Norwegian (Bokmål), Russian, Serbian, Swedish, Ukrainian and Vietnamese, "
"and all systemd man pages are now available in German."
msgstr ""

#: ../whats-new.rst:199
msgid ""
"To ensure the ``man`` command shows the documentation in your language "
"(where possible), install the right manpages-*lang* package and make sure"
" your locale is correctly configured by using"
msgstr ""

#: ../whats-new.rst:207
msgid "."
msgstr ""

#: ../whats-new.rst:212
msgid "News from Debian Med Blend"
msgstr "Novinky od Debian Med Blend"

#: ../whats-new.rst:214
msgid ""
"As in every release new packages have been added in the fields of "
"medicine and life sciences. The new package **shiny-server** might be "
"worth a particular mention, since it simplifies scientific web "
"applications using ``R``. We also kept up the effort to provide "
"Continuous Integration support for the packages maintained by the Debian "
"Med team."
msgstr ""

#: ../whats-new.rst:220
msgid ""
"The Debian Med team is always interested in feedback from users, "
"especially in the form of requests for packaging of not-yet-packaged free"
" software, or for backports from new packages or higher versions in "
"testing."
msgstr ""

#: ../whats-new.rst:225
msgid ""
"To install packages maintained by the Debian Med team, install the "
"metapackages named ``med-*``, which are at version 3.8.x for Debian "
"bookworm. Feel free to visit the `Debian Med tasks pages "
"<https://blends.debian.org/med/tasks>`__ to see the full range of "
"biological and medical software available in Debian."
msgstr ""

#: ../whats-new.rst:234
#, fuzzy
msgid "News from Debian Astro Blend"
msgstr "Novinky od Debian Med Blend"

#: ../whats-new.rst:236
msgid ""
"Debian bookworm comes with version 4.0 of the Debian Astro Pure Blend, "
"which continues to represent a great one-stop solution for professional "
"astronomers, enthusiasts and everyone who is interested in astronomy. "
"Almost all packages in Debian Astro were updated to new versions, but "
"there are also several new software packages."
msgstr ""

#: ../whats-new.rst:242
msgid ""
"For radio astronomers, the open source correlator **openvlbi** is now "
"included. The new packages **astap** and **planetary-system-stacker** are"
" useful for image stacking and astrometry resolution. A large number of "
"new drivers and libraries supporting the INDI protocol were packaged and "
"are now shipped with Debian."
msgstr ""

#: ../whats-new.rst:248
msgid ""
"The new Astropy affiliated packages **python3-extinction**, "
"**python3-sncosmo**, **python3-specreduce**, and **python3-synphot** are "
"included, as well as packages created around **python3-yt** and "
"**python3-sunpy**. Python support for the ASDF file format is much "
"extended, while the Java ecosystem is extended with libraries handling "
"the ECSV and TFCAT file formats, primarily for use with topcat."
msgstr ""

#: ../whats-new.rst:255
msgid ""
"Check `the Astro Blend page <https://blends.debian.org/astro>`__ for a "
"complete list and further information."
msgstr ""

#: ../whats-new.rst:261
msgid "Secure Boot on ARM64"
msgstr ""

#: ../whats-new.rst:263
msgid ""
"Support for Secure Boot on ARM64 has been reintroduced in |RELEASENAME|. "
"Users of UEFI-capable ARM64 hardware can boot with Secure Boot mode "
"enabled and take full advantage of the security feature. Ensure that the "
"packages **grub-efi-arm64-signed** and **shim-signed** are installed, "
"enable Secure Boot in the firmware interface of your device and reboot to"
" use your system with Secure Boot enabled."
msgstr ""

#: ../whats-new.rst:270
msgid ""
"The `Wiki <https://wiki.debian.org/SecureBoot>`__ has more information on"
" how to use and debug Secure Boot."
msgstr ""

#: ../whats-new.rst:277
msgid "Something"
msgstr ""

#: ../whats-new.rst:279
msgid "Text"
msgstr ""

#~ msgid ""
#~ "To ensure the ``man`` command shows "
#~ "the documentation in your language "
#~ "(where possible), install the right "
#~ "manpages-lang package and make sure "
#~ "your locale is correctly configured by"
#~ " using"
#~ msgstr ""

